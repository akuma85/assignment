provider "kubernetes" {
  config_path = "~/.kube/config"
}

variable "imageName" {
  type    = string
  default = "javaapp"
}

variable "name" {
  type    = string
  default = "Carl Benz"
}
variable "env_license" {
  type = string
}

locals {
  env_name = var.name
  env_license = "var.env_license"
}

resource "kubernetes_namespace" "ns" {
  metadata {
    name = "javaapp-ns"
  }
}

resource "kubernetes_deployment" "javaapp" {
  metadata {
    name      = "javaapp"
    namespace = kubernetes_namespace.ns.metadata.0.name
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "javaapp"
      }
    }
    template {
      metadata {
        labels = {
          app = "javaapp"
        }
      }
      spec {
        container {
          image ="docker.io/amitkumar1985/javaapp:latest"
          name  = "javaapp"
          port {
            container_port = 8080
          }
          env {
            name  = "name"
            value = local.env_name
          }
          env {
            name  = "license"
            value = local.env_license
          }
        }
      }
    }
  }
}

resource "null_resource" "docker_login" {
  provisioner "local-exec" {
    command = "docker login -u amitkumar1985 -p Redhat@321 https://docker.io/amitkumar1985/javaapp:latest"
  }
}

resource "null_resource" "load_image" {
  depends_on = [kubernetes_namespace.ns]

  provisioner "local-exec" {
    command = "docker load -i ${var.imageName}.tar"
  }
}

resource "kubernetes_service" "javaapp" {
  metadata {
    name      = "javaapp"
    namespace = "javaapp-ns"
  }
  spec {
    selector = {
      app = "javaapp"
    }
    port {
      name       = "http"
      port       = 8080
      target_port = 8080
    }
    type = "NodePort"
  }
}

variable "weekend" {
  type = string
  default = "no"
}

locals {
  is_weekend = var.weekend == "yes"
}

resource "kubernetes_horizontal_pod_autoscaler" "javaapp-hpa" {
  metadata {
    name = "javaapp-hpa"
    namespace = "javaapp-ns"
  }

  spec {
    scale_target_ref {
      api_version = "apps/v1"
      kind = "Deployment"
      name = "javaapp-deployment"
    }

    max_replicas = local.is_weekend ? 2 : 1
    min_replicas = local.is_weekend ? 2 : 1
    target_cpu_utilization_percentage = 80
      }
}


resource "null_resource" "post_script" {
  provisioner "local-exec" {
    command = "./expose.sh"
  }
  triggers = {
    deployment_name = kubernetes_deployment.javaapp.metadata.0.name
    service_name    = kubernetes_service.javaapp.metadata.0.name
  }
}