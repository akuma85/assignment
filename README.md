### Name: Assignment
     1. javaap.tar is used to create containerized application named "javaapp" in namespace "javaapp-ns"
     2. cd assignment
     3. git remote add origin https://gitlab.com/akuma85/assignment.git
     4. git branch -M main
     5. git push -uf origin main


### Test and Deploy
     1. Use your gitlab or Github account to clone/fork the repo.
     2. Install Minikube
     3. Install terraform
     4. Install Docker and Docker Hub/Desktop**

### Resources used
     1. Docker Hub - to store and manage the Docker image of the application.
     2. Kubernetes manifests - to define the deployment, service, and ingress resources required to run the application on the multi-tenant cluster
     3. Kubernetes Secrets - to securely store the confidential license key and make it available to the deployment as an environment variable
     4. Kubernetes Autoscaling - to automatically scale the number of replicas based on the traffic spikes on weekends
     5. Network policies - to secure network access to the application and only allow traffic from trusted sources

 
### Description of homework
     This repo contains terraformed configuration to help deploying a dockerized web application on Kubernetes Cluster (Minikube locally on Mac). It will be the application deployed in  Kuberbetes cluster running minikube. The web application is exposed on port `8080`.The application can run locally and be reachable on `http://localhost:8080/api/v1/hello`. 

     Validate:    
         Get pod running in namespace :
         kubectl get pods -n javaapp-ns
         kubectl get svc -n javaapp-ns   
         curl "http://localhost:8080/api/v1/hello" 
         curl "http://localhost:8080/actuator/metrics"
         

###  Installation: 
     1. Clone/Fork the gitlab repo https://gitlab.com/akuma85/assignment.git to your gitlab or github account.
     2. Install Terraform, Minikube and Docker HUB/Desktop account access.

###  Operating System: MacOS
     Darwin Kernel Version 22.3.0
###  Terrafrom Version:
     Terraform v1.3.7 on darwin_amd64
###  Docker version:
     Client:
     Version: 20.10.22
     Server: 
     Docker Desktop 4.16.2 (95914)
     Version: 20.10.22
###  Minikube version:
     minikube version: v1.28.0

  

###  Usage
       1. docker login -u Username -p Password
       2. docker load -i javaapp.tar
       3. docker tag acrname.azurecr.io/repo/hello DOCKERID(Repo)/javaapp:latest
       4. docker push  DOCKERID(Repo)/javaapp:latest
     ## After that :
     1. cd assignment/
     2. minikube start
     3. docker daemon start 
     4. terraform init
     5. terraform plan
     6. terraform apply
        main.tf will hold the main config.



### Roadmap
      EUS-Testing in NDA with MBA.

### Authors and acknowledgment
       Amit kumar, 
       amit.kumar099@gmail.com

### License
       Non-disclousre agreement
### Project status 
       Completed Terraformed containerized app installation
