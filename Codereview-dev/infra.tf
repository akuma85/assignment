variable "ARM_CLIENT_ID" {
  description = "Cient ID for service principal"
  type        = string
}
variable "ARM_CLIENT_SECRET" {
  description = "Client secret for service principal"
  type        = string
  sensitive   = true
}
variable "ARM_SUBSCRIPTION_ID" {
  description = "Subscription for enviroment"
  type        = string
}
variable "ARM_TENANT_ID" {
  description = "Tenant ID for enviroment"
  type        = string
}

variable "licenseKey" {
  type = string
}

variable "location" {
}

variable "main_address_space" {
}

variable "main_address_prefixes" {
}

provider "azurerm" {
  features {
    key_vault {
      purge_soft_deleted_secrets_on_destroy = true
      recover_soft_deleted_secrets          = false
    }
  }

subscription_id = var.ARM_SUBSCRIPTION_ID
client_id = var.ARM_CLIENT_ID
client_secret = var.ARM_CLIENT_SECRET
tenant_id = var.ARM_TENANT_ID

}


resource "azurerm_resource_group" "main" {
  name     = "main-dev"
  location = var.location
}

## Network
resource "azurerm_virtual_network" "main" {
  name = "main-dev"
  address_space = var.main_address_space

  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
}

resource "azurerm_subnet" "subnet_1" {
  name                 = "subnet-1"
  resource_group_name  = azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = var.main_address_prefixes

  delegation {
    name = "delegation"



  service_delegation {
  name    = "Microsoft.ContainerInstance/containerGroups"
  actions = ["Microsoft.Network/virtualNetworks/subnets/join/action", "Microsoft.Network/virtualNetworks/subnets/prepareNetworkPolicies/action"]
  }
  }
}

## NSG

resource "azurerm_network_security_group" "mainSubnet1NSG" {
  name                = "subnet-1-nsg"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  security_rule {
    name = "test123"
    priority = 100
    direction = "Inbound"
    access = "Allow"
    protocol = "Tcp"
    source_port_range = "*"
    destination_port_range = "*"
    source_address_prefix = "*"
    destination_address_prefix = "*"
  }
}

## AKS
resource "azurerm_kubernetes_cluster" "k8s" {
  name = "example-aks1"
  location = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  dns_prefix = "exampleaks1"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }
}

## KeyVault
data "azurerm_client_config" "current" {}

resource "random_id" "name" {
  byte_length = 8
}

resource "azurerm_key_vault" "vault" {
  name                       = "vault${random_id.name.hex}"
  location                   = azurerm_resource_group.main.location
  resource_group_name        = azurerm_resource_group.main.name
  tenant_id                  = data.azurerm_client_config.current.tenant_id
  sku_name                   = "premium"
  soft_delete_retention_days = 7

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    key_permissions = [
      "Create",
      "Get",
    ]

    secret_permissions = [
      "Set",
      "Get",
      "Delete",
      "Purge",
      "Recover"
    ]
  }
}

resource "azurerm_key_vault_access_policy" "csi_access_policy" {
  key_vault_id = azurerm_key_vault.vault.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = azurerm_kubernetes_cluster.k8s.kubelet_identity.0.object_id

  certificate_permissions = [
    "Get"
  ]

  key_permissions = [
    "Get"
  ]

  secret_permissions = [
    "Get"
  ]
}

resource "azurerm_key_vault_secret" "license" {
  name         = "license"
  value        = var.licenseKey
  key_vault_id = azurerm_key_vault.vault.id
}

